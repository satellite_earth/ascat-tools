ascat-tools
-----------

This is a python package with some tools to work with ASCAT bufr files. At the moment,
these tools are only used to extract some variables from the ASCAT bufr files, but more
tools can be added in further versions.


How to install:
---------------

`git clone git@gitlab.com:satellite_earth/ascat-tools.git`

`pip install .` or `python setup.py`

Examples - *Read ascat bufr file and extract values for a specific area:*
-------------------------------------------------------------------------

*file_in* = ASCAT.bufr (e.g. from OSI-SAF)

*tiles* = [lon_min, lon_max, lat_min, lat_max]
```
from ascatpy import ascat_tools

d = ascat_tools.read_ascat(file_in, tiles)

d = {"lon": [], "lat": [], "wind_speed": [], "wind_dir": [], "vector_u": [], "vector_v":[],
     "model_speed": [], "model_dir": [], "QFlag": [], "datetimes_iso_utc": []}
```

Additionally, you can save this dictionary to a geojson file and send the file by ftp:

```
ascat_tools.save_to_geojson(d, output_filename)

ascat_tools.send_file(output_filename, ftp_directory, ftp_filename, user, pass)
```