#!/usr/bin/env python
# -*- coding -*- utf-8

"""Description: This script have a main function - read_ascat(). It opens a
bufr file and check if your data is inside a input area (tiles). If yes, get
the correct values (wind speed, wind direction, latitude, longitude, iso_slot,
model direction, model speed and QFlag) with the index of wind.
After, convert the wind_speed/model_speed from m's to knot and, convert the
wind direction/model direction from meteorological convention to the
mathematical convention. Also it computes a U and V vector. Finally it writes
these parameters in a dictionary and return it if values are inside the tiles.

save_to_geojson() - save the dictionary from the read_ascat() to a geojson
                    as a FeatureCollection.

send_file() - Send a geojson from the save_to_geojson() or other file to user ftp.

    Author:
        N Simoes   16/05/2016
        I Monteiro
"""
import os
from datetime import datetime
import numpy as np
import json
from geojson import FeatureCollection
from pybufr_ecmwf import bufr


def read_ascat(file_in, tiles):
    """This script have a main function - read_ascat. Open a bufr file and check
    if your data is inside a input area (tiles). If yes, get the correct values
    (wind speed, wind direction, latitude, longitude, iso_slot, model direction,
    model speed and QFlag) with the index of wind.
    After, convert the wind_speed/model_speed from m's to knot and, convert the
    wind direction/model direction from meteorological convention to the
    mathematical convention. Also it computes a U and V vector. Finally it writes
    these parameters in a dictionary and return it if values are inside the tiles.

    :param file_in: ASCAT input file (e.g.
                    ascat_YYYYMMDD_HHMMSS_metop(N_SAT)_?????_(eps_o_coa_ovw | eps_o_250).l2_bufr)
    :param tiles: array with [lon_min, lon_max, lat_min, lat_max] e.g. [-45.00, 1.00, 28.40, 58.02]
    :return: if values aren't inside the tiles (user area) return {} empty dictionary
             else return the following dictionary
             d = {"lon": lon_ar, "lat": lat_ar, "wind_speed": np.around(ws_ar, 4),
                  "wind_dir": np.around(wd_ar2, 4), "u": u_ar, "v": v_ar,
                  "model_speed": model_s, "model_dir": model_d,
                  "QFlag": qflag, "iso_slot": tslot}
    """
    # initialize variables
    # h - hour | m - minute | lat_ar - latitue | lon_ar - longitude
    # ws_ar - wind speed | wd_ar - wind direction | model_s - model speed
    # model_d - model direction | qflag - quality flag | tslot - iso_datetimes
    h, m, lat_ar, lon_ar, ws_ar, wd_ar, model_s, model_d, qflag, tslot = (np.array([]) for i in range(10))

    # Read Filename; Get number of messages; Load messages and verbose
    bfr = bufr.BUFRReader(file_in)
    n_msg = bfr.num_msgs
    # load = bfr.msg_loaded
    verb = bfr.verbose

    # Loop over messages
    for bfr_m in bfr.messages():
        # Get names
        names = bfr_m.get_names()
        # Get number of subsets
        sbet = bfr_m.get_num_subsets()
        # Loop over subsets
        for i in range(1, int(sbet) + 1, 1):
            # Get subset values
            v = bfr_m.get_subset_values(i)
            # Check values are inside the user area
            if (tiles[2] < v[12] < tiles[3]) and (tiles[0] < v[13] < tiles[1]):
                # Get index of wind (best)
                idw = v[90]

                # initialize ws0 (wind speed) and wd0 (wind direction)
                # Get values if idw in 0.0 - 1.0 - 2.0 - 3.0 - 4.0
                # Finally, ws_ar and wd_ar will get correct values from idw
                ws0, wd0 = ([] for i in range(2))
                idw_possible_values = ["0.0", "1.0", "2.0", "3.0", "4.0"]
                if str(idw) in idw_possible_values:
                    for k in range(0, len(names), 1):
                        if str(names[k]) == 'WIND SPEED AT 10 M':
                            ws0.append(v[k])
                        if str(names[k]) == 'WIND DIRECTION AT 10 M':
                            wd0.append(v[k])

                    # Check wind dir isn't missing value
                    if wd0[int(idw)-1] > 0:
                        # Get wind speed and wind dir for the value possible
                        ws_ar = np.append(ws_ar, ws0[int(idw)-1])
                        wd_ar = np.append(wd_ar, wd0[int(idw)-1])
                        # Get latitude and longitude (append for array)
                        lat_ar = np.append(lat_ar, v[12])
                        lon_ar = np.append(lon_ar, v[13])
                        # Get iso datetimes (append for array) (more Z because is UTC)
                        # 6:Year;7:Month;8:Day;9:Hour;10:Minute;11:Seconds
                        iso_slot = datetime(int(v[6]), int(v[7]), int(v[8]), int(v[9]),
                                            int(v[10]), int(v[11])).isoformat() + "Z"
                        tslot = np.append(tslot, iso_slot)
                        # Get Model_values
                        model_s = np.append(model_s, v[84])
                        model_d = np.append(model_d, v[85])
                        # QF
                        qflag = np.append(qflag, v[88])

    # if array isn't an empty array, keep analysis
    if (ws_ar.size and wd_ar.size and lat_ar.size and lon_ar.size) != 0:
        # convert m/s to knot
        ws_ar = ws_ar * 1.9438444924574
        model_s = model_s * 1.9438444924574
        # Convert WDirection from Meteorological Convention to Mathematical Convention
        wd_ar2 = 270 - wd_ar
        model_d = 270 - model_d
        # Compute the vector U and V
        u_ar = ws_ar * np.cos(((wd_ar2*np.pi)/180.))
        v_ar = ws_ar * np.sin(((wd_ar2*np.pi)/180.))
        return {"lon": lon_ar, "lat": lat_ar, "wind_speed": np.around(ws_ar, 4),
                "wind_dir": np.around(wd_ar2, 4), "u": u_ar, "v": v_ar,
                "model_speed": model_s, "model_dir": model_d,
                "QFlag": qflag, "iso_slot": tslot}
    else:
        print("Values out of User Area: ", tiles)
        return {}


def save_to_geojson(d, file_out):
    """Save values from read_ascat() to a geojson (Feature Collection).

    :param d: dictionary from read_ascat()
              d = {"lon": lon_ar, "lat": lat_ar, "wind_speed": np.around(ws_ar, 4),
                   "wind_dir": np.around(wd_ar2, 4), "u": u_ar, "v": v_ar,
                   "model_speed": model_s, "model_dir": model_d,
                   "QFlag": qflag, "iso_slot": tslot}
    :param file_out: output filename
    :return:
    """
    # Make dictionary to construct the geojson
    # This geojson has the next parameters (wind_dir, wind_s,
    # vector u, vector v, model_dir, model_speed, qflag and iso_time)
    # ... Also lat and lon
    a_total = []
    for lat, lon, wd, ws, u, v, iso_slot, m_s, m_d, qf in \
        zip(d["lat"], d["lon"], d["wind_dir"], d["wind_speed"],
            d["u"], d["v"], d["iso_slot"], d["model_speed"], d["model_dir"], d["QFlag"]):
        a_total.append({"type": "Feature",
                        "properties": {"wind_dir": wd, "wind_s": ws, "u": u, "v": v,
                                       "model_dir": m_d, "model_speed": m_s, "QFlag": qf, "obs_dt": iso_slot},
                        "geometry": {"type": "Point", "coordinates": [lon, lat]}})
    # Create a feature collection
    features = FeatureCollection(a_total)

    # Write json file
    with open(file_out, "w") as outfile:
        json.dump(features, outfile, indent=2)


def send_file(file_to_send, ftp_dir, ftp_filename, user, passw):
    """Send geojson file to server.

    :param file_to_send: geojson file
    :param ftp_dir: ftp directory e.g. ftp://ip:/data/
    :param ftp_filename: filename for ftp
    :param user: username
    :param passw: password
    :return:
    """
    # Sent geojson by ftp
    os.system("curl -T " + file_to_send + " " + ftp_dir + ftp_filename + " --user " + user + ":" + passw)
