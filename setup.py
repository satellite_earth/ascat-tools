import os

try:
    from setuptools import setup, find_packages
except ImportError:
    from distutils.core import setup, find_packages


# Allow setup to be run from any path
os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))

# README
README = open(os.path.join(os.path.dirname(__file__), "README.md")).read()

setup(
    name='ascat-tools',
    version='0.1',
    packages=find_packages(), # exclude=('examples', 'tmp')),
    author='Nuno Simoes',
    author_email='nunosimoes58@gmail.com',
    url='https://gitlab.com/satellite_earth/ascat-tools',
    description='Read ascat bufr files; save some variables to geojson; send the geojson by ftp.',
    long_description=README,
    classifiers=[
        'Programming Language :: Python',
        'Programming Language :: Python :: 3.',
        'License :: ',
        'Operating System :: OS Independent',
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Science/Research',
        'Topic :: Scientific/Engineering',
        'Topic :: Software Development :: Libraries :: Python Modules',
    ]
)
